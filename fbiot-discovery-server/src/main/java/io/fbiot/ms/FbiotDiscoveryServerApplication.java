package io.fbiot.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FbiotDiscoveryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FbiotDiscoveryServerApplication.class, args);
	}

}
