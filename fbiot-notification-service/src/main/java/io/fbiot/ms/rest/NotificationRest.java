package io.fbiot.ms.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import io.fbiot.ms.entity.EmailRequestBody;
import io.fbiot.ms.entity.SmsRequestBody;
import io.fbiot.ms.service.NotificationService;

@RestController
@RequestMapping("/api/v1/notification")
public class NotificationRest {

	private static final Logger LOG = LoggerFactory.getLogger(NotificationRest.class);

	@Autowired
	private NotificationService notificationService;

	@GetMapping
	public String isServiceUp() {
		LOG.info("inside notification service check");
		return "Notification service running..";
	}

	
	//List<AlertMember> alertMembersList, String messageContent
	@PostMapping(value = "/send-sms", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Boolean sendSms(@RequestBody SmsRequestBody requestBody) {
		LOG.info("INSIDE SEND SMS:");
		requestBody.getAlertMembersList().stream().forEach(t->LOG.info(t.toString()));
		return notificationService.sendSms(requestBody.getAlertMembersList(), requestBody.getMessageContent());
	}

	@PostMapping(value = "/send-email", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Boolean sendEmail(@RequestBody EmailRequestBody requestBody) {
		LOG.info("INSIDE SEND EMAIL:");
		requestBody.getAlertMembersList().stream().forEach(t->LOG.info(t.toString()));
		return notificationService.sendEmail(requestBody.getAlertMembersList(), requestBody.getEmailSubject(), requestBody.getHtmlBody(), requestBody.getTextBody());
	}
	
}
