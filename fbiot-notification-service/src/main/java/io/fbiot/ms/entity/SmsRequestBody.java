package io.fbiot.ms.entity;

import java.util.List;

public class SmsRequestBody {

	private List<AlertMember> alertMembersList;
	private String messageContent;
	
	public List<AlertMember> getAlertMembersList() {
		return alertMembersList;
	}
	public void setAlertMembersList(List<AlertMember> alertMembersList) {
		this.alertMembersList = alertMembersList;
	}
	public String getMessageContent() {
		return messageContent;
	}
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	
	

}
