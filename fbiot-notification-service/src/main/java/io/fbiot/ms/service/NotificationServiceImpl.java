package io.fbiot.ms.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

import io.fbiot.ms.entity.AlertMember;

@Service
@Configuration
@ConfigurationProperties(prefix = "aws")
@ComponentScan(basePackages = { "io.fbiot" })
public class NotificationServiceImpl implements NotificationService {

	public static final Logger LOG = LoggerFactory.getLogger(NotificationServiceImpl.class);

	// @Value("${aws.accessKeyId}")
	private String accessKeyId="AKIAQSXDOZNZR5J3P2CN";

	// @Value("${aws.secretKey}")
	private String secretKey="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";

	


	// SMS
	AWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKey);

	@SuppressWarnings("deprecation")
	AmazonSNSClient snsClient = new AmazonSNSClient(credentials);

	Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();

	String smsContent = "";

	// EMAIL
	String emailSubject, htmlBody, textBody = "";

	BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKeyId, secretKey);
	String fromEmailAddress = "veeramani.karthick@fourbends.com";

	@Override
	public Boolean sendSms(List<AlertMember> alertMembersList, String messageContent) {
		// TODO Auto-generated method stub
		Boolean result = false;
		this.smsContent = messageContent;
		try {
			alertMembersList.stream().forEach(alertMember -> sendSmsImpl(alertMember));
			result = true;
		} catch (Exception e) {
			// TODO: handle exception
			result = false;
		}
		return result;
	}

	private void sendSmsImpl(AlertMember alertMember) {
		try {

			PublishResult publishResult = snsClient
					.publish(new PublishRequest().withMessage("Hi " + alertMember.getName() + " ")
							.withPhoneNumber(alertMember.getPh_no()).withMessageAttributes(smsAttributes));
			LOG.info("NOTIFICATION SMS SENT");
			LOG.info(publishResult.toString());// Prints the message ID.

		} catch (Exception e) {
			// TODO: handle exception
			LOG.info("ISSUE WITH SENDING SMS NOTIFICATION" + e.getMessage(), e);

		}

	}

	@Override
	public Boolean sendEmail(List<AlertMember> alertMembersList, String emailSubject, String HTMLBODY,
			String TEXTBODY) {
		// TODO Auto-generated method stub
		Boolean result = false;
		this.emailSubject = emailSubject;
		this.htmlBody = HTMLBODY;
		this.textBody = TEXTBODY;
		LOG.info("SENDING EMAIL NOTIFICATION");
		try {
			alertMembersList.stream().forEach(alertMember -> sendEmailImpl(alertMember));
			result = true;
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	private void sendEmailImpl(AlertMember alertMember) {
		try {
			AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).withRegion(Regions.US_WEST_2).build();
			SendEmailRequest request = new SendEmailRequest()
					.withDestination(
							new Destination().withToAddresses(alertMember.getEmail()))
					.withMessage(
							new Message()
									.withBody(
											new Body()
													.withHtml(new Content().withCharset("UTF-8")
															.withData("<p>Hi " + alertMember.getName() + "</p><br><br>"
																	+ htmlBody))
													.withText(new Content().withCharset("UTF-8").withData(textBody)))
									.withSubject(new Content().withCharset("UTF-8").withData(emailSubject)))
					.withSource(fromEmailAddress);
			client.sendEmail(request);
			LOG.info("NOTIFICATION EMAIL SENT");

		} catch (Exception ex) {
			LOG.info("ISSUE WITH SENDING EMAIL NOTIFICATION" + ex.getMessage());

		}

	}

}
