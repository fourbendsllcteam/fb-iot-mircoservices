package io.fbiot.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FbiotNotificationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FbiotNotificationServiceApplication.class, args);
	}

}
