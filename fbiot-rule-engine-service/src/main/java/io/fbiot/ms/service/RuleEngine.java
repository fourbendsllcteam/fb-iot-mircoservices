package io.fbiot.ms.service;

import com.google.gson.JsonObject;


public interface RuleEngine {
	
	public void processData(JsonObject powerMeterData, String topic);	
	

}
