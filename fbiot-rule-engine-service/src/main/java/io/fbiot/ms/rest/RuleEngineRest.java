package io.fbiot.ms.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;

import io.fbiot.ms.entity.PowerMeterData;
import io.fbiot.ms.service.RuleEngine;
import io.fbiot.ms.utils.PM_C;

@RestController
@RequestMapping("/api/v1/rule-engine")
public class RuleEngineRest {

	private static final Logger LOG = LoggerFactory.getLogger(RuleEngineRest.class);

	@Autowired
	private RuleEngine ruleEngine;

	@GetMapping
	public String isServiceUp() {
		LOG.info("inside notification service check");
		return "Ruleengine service running..";
	}	

	@PostMapping(value = "/process-data", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public void processData(@RequestBody PowerMeterData powerMeterData) {
		LOG.info("INSIDE processData:" + powerMeterData.toString());		
		
		JsonObject poweMeterJson = new JsonObject();
		poweMeterJson.addProperty(PM_C.ID, powerMeterData.getId());
		poweMeterJson.addProperty(PM_C.CREATED, powerMeterData.getCreated());
		poweMeterJson.addProperty(PM_C.CREATED_TIME, powerMeterData.getCreatedtime());
		poweMeterJson.addProperty(PM_C.BC, powerMeterData.getBc());
		poweMeterJson.addProperty(PM_C.BP, powerMeterData.getBp());
		poweMeterJson.addProperty(PM_C.BV, powerMeterData.getBv());
		poweMeterJson.addProperty(PM_C.BYV, powerMeterData.getByv());
		poweMeterJson.addProperty(PM_C.DG_ID, powerMeterData.getDg_id());
		poweMeterJson.addProperty(PM_C.GEN_ID, powerMeterData.getGen_id());
		poweMeterJson.addProperty(PM_C.FRE, powerMeterData.getFre());
		poweMeterJson.addProperty(PM_C.GEN, powerMeterData.getGen());
		poweMeterJson.addProperty(PM_C.KVA, powerMeterData.getKva());
		poweMeterJson.addProperty(PM_C.KWH, powerMeterData.getKwh());
		poweMeterJson.addProperty(PM_C.METER_ID, powerMeterData.getMeter_id());
		poweMeterJson.addProperty(PM_C.PF, powerMeterData.getPf());
		poweMeterJson.addProperty(PM_C.RBV, powerMeterData.getRbv());
		poweMeterJson.addProperty(PM_C.RC, powerMeterData.getRc());
		poweMeterJson.addProperty(PM_C.RP, powerMeterData.getRp());
		poweMeterJson.addProperty(PM_C.RV, powerMeterData.getRv());
		poweMeterJson.addProperty(PM_C.TC, powerMeterData.getTc());
		poweMeterJson.addProperty(PM_C.TP, powerMeterData.getTp());
		poweMeterJson.addProperty(PM_C.TPV, powerMeterData.getTpv());
		poweMeterJson.addProperty(PM_C.TV, powerMeterData.getTv());
		poweMeterJson.addProperty(PM_C.YC, powerMeterData.getYc());
		poweMeterJson.addProperty(PM_C.YP, powerMeterData.getYp());
		poweMeterJson.addProperty(PM_C.YRV, powerMeterData.getYrv());
		poweMeterJson.addProperty(PM_C.YV, powerMeterData.getYv());		
		
		ruleEngine.processData(poweMeterJson, powerMeterData.getTopic());
	}
	
}
