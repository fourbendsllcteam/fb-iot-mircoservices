package io.fbiot.ms.utils;

public interface KWH_C {

	public static final String ID = "id", CREATEDTIME = "createdtime", DEVICE_ID = "device_id", DG_ID = "dg_id",
			DG_STATUS = "dg_status", KVA_END = "kva_end", KVA_START = "kva_start", KWH_END = "kwh_end",
			KWH_START = "kwh_start", LAST_OFF_TIME = "last_off_time", LAST_ON_TIME = "last_on_time";

}
