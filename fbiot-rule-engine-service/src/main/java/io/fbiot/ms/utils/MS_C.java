package io.fbiot.ms.utils;

public interface MS_C {

	public static final String ID = "id", CREATEDTIME = "createdtime", DG = "dg", DG_ID = "dg_id",
			METER_ID = "meter_id", MSG_ID = "msg_id", RECORD_ID = "record_id", STATUS = "status",
			UPDATEDTIME = "updatedtime";

}
