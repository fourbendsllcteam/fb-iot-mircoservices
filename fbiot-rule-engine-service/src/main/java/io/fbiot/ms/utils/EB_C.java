package io.fbiot.ms.utils;

public interface EB_C {

	public static final String ID = "id", DG_ID = "dg_id",
			METER_ID = "meter_id", STATUS = "status", STATUS_OFF = "status_off", STATUS_ON = "status_on";
	
}
