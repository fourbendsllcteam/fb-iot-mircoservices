package io.fbiot.ms;

import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Component;

/*
 * @PropertySource(value = "classpath:application.properties")
 * 
 * @EnableElasticsearchRepositories(basePackages =
 * "io.fb.iotplatform.repository")
 */

@Component
@ConfigurationProperties("elasticsearch")
@Configuration
@ComponentScan(basePackages = { "io.fbiot.ms" }) 
public class ElasticSearchConfig {
	
	public static final Logger LOG = LoggerFactory.getLogger(ElasticSearchConfig.class);

	private String host;	
	private int port;
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@Bean
	RestHighLevelClient elasticSearchClient() {
		try {
			ClientConfiguration clientConfiguration = ClientConfiguration.builder().connectedTo(host + ":" + port)
					.build();
			LOG.info("Connected to elastic search succesfully... " + host+":"+port);
			return RestClients.create(clientConfiguration).rest();
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Error in elastic search connection establishment", e.getMessage(), e);
			return null;
		}
		
	}

	
	  @Bean public ElasticsearchOperations elasticsearchTemplate() { return new
	  ElasticsearchRestTemplate(elasticSearchClient()); }
	 
	
	
	@Profile("dev")
	@Bean
	public void devDatabaseConnection() {
		LOG.info("ES connection for DEV");
		LOG.info(host + ":" +  port);		
	}


	@Profile("prod")
	@Bean
	public void prodDatabaseConnection() {
		LOG.info("ES connection for PROD");
		LOG.info(host + ":" +  port);	
	} 

}
