package io.fbiot.ms.entity;

import java.util.List;

public class EmailRequestBody {

	private List<AlertMember> alertMembersList;
	private String emailSubject;
	private String htmlBody;
	private String textBody;
	
	public List<AlertMember> getAlertMembersList() {
		return alertMembersList;
	}
	public void setAlertMembersList(List<AlertMember> alertMembersList) {
		this.alertMembersList = alertMembersList;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public String getHtmlBody() {
		return htmlBody;
	}
	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}
	public String getTextBody() {
		return textBody;
	}
	public void setTextBody(String textBody) {
		this.textBody = textBody;
	}
	

}
