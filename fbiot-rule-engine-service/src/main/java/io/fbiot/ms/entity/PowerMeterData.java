package io.fbiot.ms.entity;

public class PowerMeterData {

	private String id;
	private String topic;
	private float bc;
	private float bp;
	private float bv;
	private float byv;
	private Long created;
	private Long createdtime;

	private Integer dg_id;
	private Integer gen_id;

	private float fre;
	private Integer gen;
	private float kva;
	private float kwh;
	private Integer meter_id;
	private float pf;
	private float rbv;
	private float rc;
	private float rp;
	private float rv;
	private float tc;
	private float tp;
	private float tpv;
	private float tv;
	private float yc;
	private float yp;
	private float yrv;
	private float yv;

	public PowerMeterData() {

	}

	public PowerMeterData(String id, float bc, float bp, float bv, float byv, Long created, Long createdtime,
			Integer dg_id, float fre, Integer gen, float kva, float kwh, Integer meter_id, float pf, float rbv,
			float rc, float rp, float rv, float tc, float tp, float tpv, float tv, float yc, float yp, float yrv,
			float yv) {
		super();
		this.id = id;
		this.bc = bc;
		this.bp = bp;
		this.bv = bv;
		this.byv = byv;
		this.created = created;
		this.createdtime = createdtime;
		this.gen_id = dg_id;
		this.fre = fre;
		this.gen = gen;
		this.kva = kva;
		this.kwh = kwh;
		this.meter_id = meter_id;
		this.pf = pf;
		this.rbv = rbv;
		this.rc = rc;
		this.rp = rp;
		this.rv = rv;
		this.tc = tc;
		this.tp = tp;
		this.tpv = tpv;
		this.tv = tv;
		this.yc = yc;
		this.yp = yp;
		this.yrv = yrv;
		this.yv = yv;
	}

	public Integer getGen_id() {
		return gen_id;
	}

	public void setGen_id(Integer gen_id) {
		this.gen_id = gen_id;
	}
	
	

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public float getBc() {
		return bc;
	}

	public void setBc(float bc) {
		this.bc = bc;
	}

	public float getBp() {
		return bp;
	}

	public void setBp(float bp) {
		this.bp = bp;
	}

	public float getBv() {
		return bv;
	}

	public void setBv(float bv) {
		this.bv = bv;
	}

	public float getByv() {
		return byv;
	}

	public void setByv(float byv) {
		this.byv = byv;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(Long createdtime) {
		this.createdtime = createdtime;
	}

	public Integer getDg_id() {
		return gen_id;
	}

	public void setDg_id(Integer dg_id) {
		this.gen_id = dg_id;
	}

	public float getFre() {
		return fre;
	}

	public void setFre(float fre) {
		this.fre = fre;
	}

	public Integer getGen() {
		return gen;
	}

	public void setGen(Integer gen) {
		this.gen = gen;
	}

	public float getKva() {
		return kva;
	}

	public void setKva(float kva) {
		this.kva = kva;
	}

	public float getKwh() {
		return kwh;
	}

	public void setKwh(float kwh) {
		this.kwh = kwh;
	}

	public Integer getMeter_id() {
		return meter_id;
	}

	public void setMeter_id(Integer meter_id) {
		this.meter_id = meter_id;
	}

	public float getPf() {
		return pf;
	}

	public void setPf(float pf) {
		this.pf = pf;
	}

	public float getRbv() {
		return rbv;
	}

	public void setRbv(float rbv) {
		this.rbv = rbv;
	}

	public float getRc() {
		return rc;
	}

	public void setRc(float rc) {
		this.rc = rc;
	}

	public float getRp() {
		return rp;
	}

	public void setRp(float rp) {
		this.rp = rp;
	}

	public float getRv() {
		return rv;
	}

	public void setRv(float rv) {
		this.rv = rv;
	}

	public float getTc() {
		return tc;
	}

	public void setTc(float tc) {
		this.tc = tc;
	}

	public float getTp() {
		return tp;
	}

	public void setTp(float tp) {
		this.tp = tp;
	}

	public float getTpv() {
		return tpv;
	}

	public void setTpv(float tpv) {
		this.tpv = tpv;
	}

	public float getTv() {
		return tv;
	}

	public void setTv(float tv) {
		this.tv = tv;
	}

	public float getYc() {
		return yc;
	}

	public void setYc(float yc) {
		this.yc = yc;
	}

	public float getYp() {
		return yp;
	}

	public void setYp(float yp) {
		this.yp = yp;
	}

	public float getYrv() {
		return yrv;
	}

	public void setYrv(float yrv) {
		this.yrv = yrv;
	}

	public float getYv() {
		return yv;
	}

	public void setYv(float yv) {
		this.yv = yv;
	}

	@Override
	public String toString() {
		return "PowerMeterData [id=" + id + ", bc=" + bc + ", bp=" + bp + ", bv=" + bv + ", byv=" + byv + ", created="
				+ created + ", createdtime=" + createdtime + ", dg_id=" + gen_id + ", fre=" + fre + ", gen=" + gen
				+ ", kva=" + kva + ", kwh=" + kwh + ", meter_id=" + meter_id + ", pf=" + pf + ", rbv=" + rbv + ", rc="
				+ rc + ", rp=" + rp + ", rv=" + rv + ", tc=" + tc + ", tp=" + tp + ", tpv=" + tpv + ", tv=" + tv
				+ ", yc=" + yc + ", yp=" + yp + ", yrv=" + yrv + ", yv=" + yv + "]";
	}

}
