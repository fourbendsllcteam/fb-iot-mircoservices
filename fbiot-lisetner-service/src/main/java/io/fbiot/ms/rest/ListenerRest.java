package io.fbiot.ms.rest;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.fbiot.ms.mqtt.MQTTSubscriber;
import io.fbiot.ms.utils.PM_C;



@RestController
public class ListenerRest {

	@Autowired
	MQTTSubscriber subscriber;


	public static final Logger LOG = LoggerFactory.getLogger(ListenerRest.class);

	@RequestMapping("/")
	public String checkApplication() {

		String result = "FB IOT Platform.. listening to MQTT...";

		return result;
	}

	@RequestMapping("/subscribe")
	public String subscribe(@RequestParam(name = "topic") String topic) {
		try {
			subscriber.subscribeMessage(topic);
			return String.format("Subscribed to %s sucessfully", topic);
		} catch (Exception e) {
			// TODO: handle exception

			return String.format("Issue with subscribing.. " + e.getStackTrace());
		}

	}

	@RequestMapping("/unsubscribe")
	public String unSubscribe(@RequestParam(name = "topic") String topic) {
		subscriber.unsubscribeMessage(topic);
		return String.format("Unsubscribed to %s sucessfully", topic);
	}

	@RequestMapping("/dummy-call")
	public String triggerDummyCall(@RequestParam(name = "gen") String genValue,
			@RequestParam(name = "meterid") String meterId, @RequestParam(name = "topic") String topic) {

		String jsonResponse = "{\"rv\":235.24,\"bv\":240.23,\"yv\":233.32,\"tv\":236.263,\"rbv\":475.57,\"byv\":473.55,\"yrv\":468.56,\"tpv\":472.56,\"rc\":0,\"bc\":0,\"yc\":0,\"tc\":0,\"rp\":0,\"bp\":0,\"yp\":0,\"tp\":0,\"rkva\":0,\"bkva\":0,\"ykva\":0,\"tkva\":0,\"pf\":0.98,\"fre\":59.6,\"kwh\":125,\"gen\":1,\"gen_id\":\"12345\",\"meter_id\":501}";

		try {

			JsonObject currentPowerMeterData = JsonParser.parseString(jsonResponse).getAsJsonObject();
			currentPowerMeterData.addProperty(PM_C.GEN, genValue);
			currentPowerMeterData.addProperty(PM_C.METER_ID, meterId);

			currentPowerMeterData.addProperty(PM_C.ID, UUID.randomUUID().toString());
			if (currentPowerMeterData.get(PM_C.GEN_ID) == null)
				currentPowerMeterData.addProperty(PM_C.DG_ID, 0);
			else
				currentPowerMeterData.addProperty(PM_C.DG_ID, currentPowerMeterData.get(PM_C.GEN_ID).getAsInt());

			//ruleEngine.processData(currentPowerMeterData, topic);
			return "Done parsing..!!";
		} catch (Exception e) {
			// TODO: handle exception
			LOG.info("Data can't be sent to rule engine" + e.getMessage(), e);
			return "Data can't be sent to rule engine";
		}

	}
}
