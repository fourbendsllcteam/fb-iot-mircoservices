package io.fbiot.ms.mqtt;

import java.io.FileReader;
import java.sql.Timestamp;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.core.env.Environment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.json.simple.parser.JSONParser;

import io.fbiot.ms.entity.PowerMeterData;
import io.fbiot.ms.utils.PM_C;

@Configuration
@ComponentScan(basePackages = "io.fbiot.ms*")
@PropertySource("classpath:application.properties")
@Component
public class MQTTSubscriber implements MqttCallback {

	private static final Logger LOG = LoggerFactory.getLogger(MQTTSubscriber.class);

	private MqttClient mqttClient = null;

	@Autowired
	private Environment environment;

	@Value("${client.data}")
	private String clientDataFileName;
	
	@Value("${client.id}")
	private String clientId;// = "dev-client-data.json";

	@Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
    public void init() {
		this.config(clientId, clientDataFileName);
    }



	@Override
	public void connectionLost(Throwable cause) {
		LOG.info("Connection Lost");
		LOG.info("Causes:" + cause);
	}

	public void subscribeMessage(String topic) {
		try {
			this.mqttClient.subscribe(topic, 2);
			LOG.info(String.format("Subscribed to %s sucessfully", topic));
		} catch (MqttException me) {
			LOG.info("Issue with subscription" + me.getMessage(), me);
		}
	}

	@SuppressWarnings("unchecked")
	public void subscribeDefaults(String fileName) {
		try {
			LOG.info("JSON FILE: " + fileName);
			Object obj = new JSONParser().parse(new FileReader(fileName));
			JSONObject jo = (JSONObject) obj;
			JSONArray topics = (JSONArray) jo.get("subscription-topics");
			topics.stream().forEach(t -> subscribeMessage((String) t));
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Can't connect to MQTT, check broker is running", e.getMessage(), e);
		}
	}

	public void unsubscribeMessage(String topic) {
		// TODO Auto-generated method stub
		try {
			this.mqttClient.unsubscribe(topic);
			LOG.info(String.format("Unsubscribed to %s sucessfully", topic));
		} catch (MqttException me) {
			LOG.info("Issue with unsubscription" + me.getMessage(), me);

		}
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

		LOG.info("***********************************************************************");
		LOG.info("Message Arrived at Time: " + new Timestamp(System.currentTimeMillis()).toString() + "  Topic: "
				+ topic + "  Message: " + new String(mqttMessage.getPayload()));
		LOG.info("***********************************************************************");

		String jsonResponse = new String(mqttMessage.getPayload());
		Gson g = new Gson();

		try {

			String id = UUID.randomUUID().toString();

			PowerMeterData powerMeterData = g.fromJson(jsonResponse, PowerMeterData.class);
			powerMeterData.setTopic(topic);
			powerMeterData.setId(id);

			JsonObject currentPowerMeterData = JsonParser.parseString(jsonResponse).getAsJsonObject();
			currentPowerMeterData.addProperty("topic", topic);
			currentPowerMeterData.addProperty(PM_C.ID, id);

			if (currentPowerMeterData.get(PM_C.GEN_ID) == null) {
				powerMeterData.setDg_id(0);
				currentPowerMeterData.addProperty(PM_C.DG_ID, 0);
			} else {
				powerMeterData.setDg_id(currentPowerMeterData.get(PM_C.GEN_ID).getAsInt());
				currentPowerMeterData.addProperty(PM_C.DG_ID, currentPowerMeterData.get(PM_C.GEN_ID).getAsInt());
			}

			restTemplate.postForObject("http://localhost:8102/api/v1/rule-engine/process-data", powerMeterData,
					PowerMeterData.class);
			
			restTemplate.postForObject("http://fbiot-rule-engine/api/v1/rule-engine/process-data", powerMeterData,
					PowerMeterData.class);

		} catch (Exception e) {
			LOG.info("Data can't be sent to rule engine" + e.getMessage());
			LOG.debug(e.getMessage(), e);

		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

	public void config(String clientId, String clientDataFileName) {
		LOG.info("Testt**************");		
		

		String brokerUrl = "tcp://demopms.4blabs.com:1883";
		MemoryPersistence persistence = new MemoryPersistence();
		MqttConnectOptions connectionOptions = new MqttConnectOptions();
		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence); // localClientUdhaya2
																				// //fbiotMsClient1
			connectionOptions.setCleanSession(false);
			connectionOptions.setAutomaticReconnect(true);
			mqttClient.connect(connectionOptions);
			mqttClient.setCallback(this);

			subscribeDefaults(clientDataFileName);
		} catch (MqttException me) {
			LOG.error("Connection Error: " + me.getMessage(), me);
		}

	}

}
